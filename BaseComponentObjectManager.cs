﻿public class BaseComponentObjectManager : BaseComponentObject
{
    // This way the manager has access to every other components
    // That are needed for further control and dependency injection
    private ComponentObjectServer _managedServer;

    public override void Init()
    {
        base.Init();
        _managedServer = GetComponent<ComponentObjectServer>();
    }

    public override void Inject()
    {
        base.Inject();
    }

    public T GetComponentObjectManaged<T>()
    {
        return _managedServer.GetComponentObject<T>();
    }
}
