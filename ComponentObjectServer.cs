﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// This has the role of retaining all the components
/// that are then injected into a GameObject and all components
/// in this list will have access to all components in every ComponentObject
/// </summary>
public class ComponentObjectServer : MonoBehaviour
{
	[SerializeField]
	private GameObject[] _compPrefabs;

	private List<BaseComponentObject> _compObjects;
	private List<ComponentObjectServer> _servers;

	new private Transform transform;

	public bool IsInitialized
	{
		private set;
		get;
	}

	public IEnumerator InitServer()
	{
		IsInitialized = false;
		transform = base.GetComponent<Transform>();
		_compObjects = new List<BaseComponentObject>();
		_servers = new List<ComponentObjectServer>();
		var exObjects = GetComponentsInChildren<Transform>();
		yield return StartCoroutine(initRoutine(exObjects, transform));
	}

	public IEnumerator InitAll()
	{
		for (int i = 0; i < _compObjects.Count; i++)
		{
			_compObjects[i].Init();
		}

		for (int i = 0; i < _servers.Count; i++)
		{
			yield return StartCoroutine(_servers[i].InitAll());
		}
	}

	public IEnumerator InjectAll()
	{
		for (int i = 0; i < _compObjects.Count; i++)
		{
			_compObjects[i].Inject();
		}

		for (int i = 0; i < _servers.Count; i++)
		{
			yield return StartCoroutine(_servers[i].InjectAll());
		}
	}

	public IEnumerator AwakeAll()
	{
		for (int i = 0; i < _compObjects.Count; i++)
		{
			_compObjects[i].COAwake();
		}

		for (int i = 0; i < _servers.Count; i++)
		{
			yield return StartCoroutine(_servers[i].AwakeAll());
		}
	}

	public IEnumerator StartAll()
	{
		for (int i = 0; i < _compObjects.Count; i++)
		{
			_compObjects[i].COStart();
		}

		for (int i = 0; i < _servers.Count; i++)
		{
			yield return StartCoroutine(_servers[i].StartAll());
		}
	}

	private IEnumerator initRoutine(Transform[] existingObjects, Transform parent)
	{
		bool isInSceneAlready = false;
		for (int i = 0; i < _compPrefabs.Length; i++)
		{
			int existingObjectIndex = -1;
			isInSceneAlready = false;
			for (int j = 0; j < existingObjects.Length; j++)
			{
				if (_compPrefabs[i].name == existingObjects[j].name)
				{
					isInSceneAlready = true;
					existingObjectIndex = j;
					break;
				}
			}

			if (isInSceneAlready)
			{
				_compObjects.Add(existingObjects[existingObjectIndex].GetComponent<BaseComponentObject>());
				_compObjects[i].InitBase(this);
				var maybeServer = existingObjects[existingObjectIndex].GetComponent<ComponentObjectServer>();
				if (maybeServer != null)
				{
					_servers.Add(maybeServer);
				}
			}
			else
			{
				var go = GameObject.Instantiate(_compPrefabs[i]);
				go.transform.SetParent(parent);
				var trans = go.transform;
				var rectTrans = go.GetComponent<RectTransform>();
				var preTrans = _compPrefabs[i].transform;
				trans.localPosition = preTrans.localPosition;
				trans.localRotation = preTrans.localRotation;
				trans.localScale = preTrans.localScale;
				var preRectTrans = _compPrefabs[i].GetComponent<RectTransform>();
				if (preRectTrans)
				{
					rectTrans.anchoredPosition3D = preRectTrans.anchoredPosition3D;
					rectTrans.anchorMax = preRectTrans.anchorMax;
					rectTrans.anchorMin = preRectTrans.anchorMin;
					rectTrans.sizeDelta = preRectTrans.sizeDelta;
				}
				go.name = _compPrefabs[i].name;
				_compObjects.Add(go.GetComponent<BaseComponentObject>());
				_compObjects[i].InitBase(this);
				var maybeServer = go.GetComponent<ComponentObjectServer>();
				if (maybeServer != null)
				{
					_servers.Add(maybeServer);
				}
			}

			yield return null;
		}

		for (int i = 0; i < _servers.Count; i++)
		{
			yield return StartCoroutine(_servers[i].InitServer());
		}

		IsInitialized = true;
	}

	public T GetComponentObject<T>()
	{
		for (int i = 0; i < _compObjects.Count; i++)
		{
			var comp = _compObjects[i];
			if (comp is T)
			{
				return (T)System.Convert.ChangeType(comp, typeof(T));
			}
		}

		return default(T);
	}

	public void ConfigureGameObject(GameObject go)
	{
		_compObjects.Add(go.GetComponent<BaseComponentObject>());
		_compObjects[_compObjects.Count - 1].InitBase(this);
		var maybeServer = go.GetComponent<ComponentObjectServer>();
		if (maybeServer != null)
		{
			_servers.Add(maybeServer);
			StartCoroutine(ConfigureRoutine(_compObjects[_compObjects.Count - 1], maybeServer));
		}
		else
		{
			StartCoroutine(ConfigureRoutine(_compObjects[_compObjects.Count - 1]));
		}
	}

	IEnumerator ConfigureRoutine(BaseComponentObject bco, ComponentObjectServer server = null)
	{
		if (server)
			yield return StartCoroutine(server.InitServer());

		bco.Init();
		if (server)
			yield return StartCoroutine(server.InitAll());

		bco.Inject();
		if (server)
			yield return StartCoroutine(server.InjectAll());

		bco.COAwake();
		if (server)
			yield return StartCoroutine(server.AwakeAll());

		bco.COStart();
		if (server)
			yield return StartCoroutine(server.StartAll());

		yield return null;
	}
}
