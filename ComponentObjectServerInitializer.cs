﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ComponentObjectServer))]
public class ComponentObjectServerInitializer : MonoBehaviour
{
	// Use this for initialization
	void Awake ()
	{
		var server = GetComponent<ComponentObjectServer>();
		StartCoroutine(InitRoutine(server));
	}

	IEnumerator InitRoutine(ComponentObjectServer server)
	{
		yield return StartCoroutine(server.InitServer());
		yield return StartCoroutine(server.InitAll());
		yield return StartCoroutine(server.InjectAll());
		yield return StartCoroutine(server.AwakeAll());
		yield return StartCoroutine(server.StartAll());
	}
}
