﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class BaseComponentObject : MonoBehaviour
{
	private ComponentObjectServer _server;
	private Component[] _components;
	private MonoBehaviour[] _dependencies;
	private Type[] _requiredDependecyObjects;

	private bool _isInitialized = false;
	/// <summary>
    /// This is used by the <see cref = "ComponentObjectServer"/>
	/// for registering itself.
    /// </summary>
    /// <param name="server">ComponentObjectServer to register.</param>
	public void InitBase(ComponentObjectServer server)
	{
		_server = server;
	}

	public void RegisterToServer(GameObject go)
	{
		_server.ConfigureGameObject(go);
	}

	public virtual void Init()
	{
		var components = GetComponents<Component>();
		_components = components;
		_isInitialized = true;
	}

	/// <summary>
	/// This gets the first component found in any of the other
	/// game objects that are childs of the same parent gameObject
	/// that works as a server
	/// </summary>
	/// <typeparam name="T">A class derived from BaseComponentObject</typeparam>
	/// <returns></returns>
	public T GetComponentObject<T>()
	{
		return _server.GetComponentObject<T>();
	}

	/// <summary>
	/// This gets the first found component only in the current game object
	/// </summary>
	/// <typeparam name="T">A class derived from BaseComponentObject</typeparam>
	/// <returns></returns>
	public T GetLocalComponent<T>()
	{
		T comp = (T)findComponent<T>();
		//Debug.Log(name + "[component = "+ comp.GetType().ToString() +"] = " + comp.ToString());
		return comp; //is ComponentObjectServer ? default(T) : comp;
	}

	///<summary>
	/// This finds the first Monobehaviour component
	/// of the same type T.
	///</summary>
	///<returns>A reference of type T</returns>
	private object findComponent<T>()
	{
		for (int i = 0; i < _components.Length; i++)
		{
			if (_components[i] is T)
			{
				return _components[i];
			}
		}
		return null;
	}

	public bool IsInitialized()
	{
		if (_server != null)
			return _server.IsInitialized && _isInitialized && gotAllDependencies();
		else
			return false;
	}

#region Dependency Injection Methods
	/// <summary>
    /// Used by BaseComponentManager to inject the necessary dependencies.
    /// </summary>
	public virtual void Inject()
	{

	}
	/// <summary>
    /// This method is overriden only when the current object has some dependencies.
    /// </summary>
    /// <returns>Array of object types</returns>
	public virtual Type[] GetRequiredDependencyObjects()
	{
		return null;
	}

	/// <summary>
    /// Used for Dependency injection of objects that a component needs in order to start functioning properly.
    /// </summary>
    /// <param name="dep"></param>
	public void AddDependency<T>(MonoBehaviour dep)
	{
		addDependency<T>(dep, _requiredDependecyObjects, _dependencies);
	}

	public T GetDependency<T>()
	{
		return getDependency<T>(_dependencies);
	}

	private void addDependency<T>(MonoBehaviour dep, Type[] requiredDependencies, MonoBehaviour[] dependencies)
	{
		if (requiredDependencies != null)
		{
			for (int i = 0; i < requiredDependencies.Length; i++)
			{
				if (requiredDependencies[i] == typeof(T))
				{
					//var monoDep = (MonoBehaviour)System.Convert.ChangeType(dep, typeof(MonoBehaviour));
					_addDependency(dep, dependencies, i);
				}
			}
		}
	}

	private void _addDependency(MonoBehaviour dependency, MonoBehaviour[] dependencies, int index)
	{
		dependencies[index] = dependency;
	}

	private T getDependency<T>(MonoBehaviour[] dependencies)
	{
		if (dependencies != null)
		{
			for (int i = 0; i < dependencies.Length; i++)
			{
				if (dependencies[i] is T)
				{
					return (T)System.Convert.ChangeType(dependencies[i], typeof(T));
				}
			}
		}
		return default(T);
	}

	private bool gotAllDependencies()
	{
		return 
			_gotDependencies(_requiredDependecyObjects, _dependencies);
	}

	private bool _gotDependencies(Type[] requiredDependencyObjects, MonoBehaviour[] dependencies)
	{
		if (requiredDependencyObjects != null && dependencies != null)
		{
			bool gotAll = true;
			for (int i = 0; i < dependencies.Length; i++)
			{
				if (dependencies[i] == null)
				{
					gotAll = false;
					break;
				}
			}
			return gotAll;
		}
		else if (requiredDependencyObjects == null)
		{
			return true;
		}

		return false;
	}
#endregion

#region Alternative to Monobehaviour methods
	public virtual void COAwake()
	{

	}
	public virtual void COStart()
	{
	}

	public virtual void COUpdate()
	{
	}

	public virtual void COFixedUpdate()
	{
	}
#endregion

#region MonoBehaviour methods
	void Awake()
	{
		_requiredDependecyObjects = GetRequiredDependencyObjects();

		if (_requiredDependecyObjects != null)
		{
			_dependencies = new MonoBehaviour[_requiredDependecyObjects.Length];
		}
	}

	void Update()
	{
		if (!IsInitialized())
			return;
		COUpdate();
	}

	void FixedUpdate()
	{
		if (!IsInitialized())
			return;
		COFixedUpdate();
	}
#endregion
}
